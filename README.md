# 微信小程序导航golang版

### 特点
1. 基于gin web 框架开发
2. 基于gorm框架 数据库使用mysql
3. 基于cache2go数据缓存
4. 基于pongo2 模版引擎 布局采用bootstrap3开发


### 特别说明
1. 本项目所有小程序信息经过二次加工处理, 静态资源搜集来自凌夕网络 知晓程序网站，演示信息仅供测试使用，使用后请自行销毁
2. 本项目使用大量第三方框架，如果涉及侵权问题，请作者联系我删除相关代码 联系方式 itlabers@gmail.com
3. 部分业务逻辑需要使用者应根据自身业务特点完成

### 版权归属 武汉点讯科技有限公司

#### Apache License Version 2.0
#### 微信小程序导航golang版 初始知识产权归武汉点讯科技有限公司所有，任何人和企业可以无偿使用，可以对产品、源代码进行任何形式的修改，可以打包在其他产品中进行销售。
#### 任何使用了微信小程序导航golang版的全部或部分功能产品或文章等形式的成果必须显式注明  武汉点讯科技有限公司 版权所有

### 鸣谢
#### 感谢下列优秀开源项目：

* https://github.com/flosch/pongo2
* https://github.com/gin-gonic/gin
* https://github.com/robvdl/pongo2gin
* https://github.com/cihub/seelog
* https://github.com/muesli/cache2go
* https://github.com/jinzhu/gorm (国产)
* https://github.com/disintegration/imaging

##### 感谢重庆暄洁的梁先生，愿意提供阿里云服务器作为演示资源。
##### 当前项目模板可以作为golang程序员的入门参考，同时强调当前项目只有初始形态，并不完善，归根结底由于作者精力有限，以及前端，UI设计能力不足，武汉点讯作为刚起步的创业公司，真诚希望得到更多优秀的前端/UI设计/产品经理的垂青。
##### 武汉点讯科技希望借助开源的力量在武汉地区推广和普及golang,欢迎golang程序员加入兴趣联盟,拥抱开源,有意者可以联系itlabers@gmai.com或者加微信spirit_demon 说明来意。


### 演示地址
##### http://123.57.243.58

### 项目构建
#####  项目基于 gb  vendor 构建, 一个Golang项目工程通常由bin、pkg、src三个子目录构成，gb在这个概念的基础上新增了一个vendor目录来存放项目依赖的第三方包；一个gb项目的工作目录里包含该项目需要的所有Go代码。
#####  gb项目不放在你的$GOPATH中，也不需要为你的gb项目设置或修改$GOPATH。依赖的第三包需要放到vendor/src目录中，并使用gb来编译和测试你的项目。
#####  gb 使用相关教程  https://segmentfault.com/a/1190000004346513
#####  gb 官网地址  https://getgb.io/

#####  微信小程序的数据库初始化脚步  miniapp.sql 初始化逻辑  initDb()
#####  由于微信小程序二维码，截图资源图片过大，无法上传到git上,一次演示数据图片资源可以调crawler包下miniapp_test自行采集 ，请注意测试的请求量，不要对其他网站造成恶意攻击。



