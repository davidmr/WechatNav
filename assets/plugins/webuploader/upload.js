/**
 * Created by zchuanzhao on 16/6/12.
 */
// 文件上传
jQuery(function() {
    var $ = jQuery,
        $list1 = $('#imagesList1'),
        $btn1 = $('#startBtn1'),
        uploader1,

        $list2 = $('#imagesList2'),
        $btn2 = $('#startBtn2'),
        uploader2,

        $list3 = $('#imagesList3'),
        $btn3 = $('#startBtn3'),
        uploader3,

        $list4 = $('#imagesList4'),
        $btn4 = $('#startBtn4'),
        uploader4,

        $list5 = $('#imagesList5'),
        $btn5 = $('#startBtn5'),
        uploader5,

        $list6 = $('#imagesList6'),
        $btn6 = $('#startBtn6'),
        uploader6,

        $list7 = $('#imagesList7'),
        $btn7 = $('#startBtn7'),
        uploader7,
        state = 'pending';


    uploader1 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker1',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader1.on( 'fileQueued', function( file ) {
        $list1.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader1.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader1.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#ico' ).val(json.filename);
            $("#ico-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader1.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader1.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader1.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn1.text('暂停上传');
        } else {
            $btn1.text('开始上传');
        }
    });

    $btn1.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader1.stop();
        } else {
            uploader1.upload();
        }
    });





    uploader2 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker2',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader2.on( 'fileQueued', function( file ) {
        $list2.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader2.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader2.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#ercode' ).val(json.filename);
            $("#ercode-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader2.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader2.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader2.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn1.text('暂停上传');
        } else {
            $btn1.text('开始上传');
        }
    });

    $btn2.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader2.stop();
        } else {
            uploader2.upload();
        }
    });


    uploader3 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker3',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader3.on( 'fileQueued', function( file ) {
        $list3.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader3.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader3.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#pic1' ).val(json.filename);
            $("#pic1-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader3.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader3.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader3.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn3.text('暂停上传');
        } else {
            $btn3.text('开始上传');
        }
    });

    $btn3.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader3.stop();
        } else {
            uploader3.upload();
        }
    });





    uploader4 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker4',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader4.on( 'fileQueued', function( file ) {
        $list4.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader4.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader4.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#pic2' ).val(json.filename);
            $("#pic2-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader4.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader4.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader4.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn4.text('暂停上传');
        } else {
            $btn4.text('开始上传');
        }
    });

    $btn4.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader4.stop();
        } else {
            uploader4.upload();
        }
    });





    uploader5 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/assets/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker5',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader5.on( 'fileQueued', function( file ) {
        $list5.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader5.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader5.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#pic3' ).val(json.filename);
            $("#pic3-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader5.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader5.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader5.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn5.text('暂停上传');
        } else {
            $btn5.text('开始上传');
        }
    });

    $btn5.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader5.stop();
        } else {
            uploader5.upload();
        }
    });






    uploader6 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/assets/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker6',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader6.on( 'fileQueued', function( file ) {
        $list6.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader6.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader6.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#pic4' ).val(json.filename);
            $("#pic4-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader6.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader6.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader6.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn6.text('暂停上传');
        } else {
            $btn6.text('开始上传');
        }
    });

    $btn6.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader6.stop();
        } else {
            uploader6.upload();
        }
    });






    uploader7 = WebUploader.create({
        // 不压缩image
        resize: false,
        auto: true,
        // swf文件路径
        swf: basePath+"/assets/plugins/webuploader/Uploader.swf",
        // 文件接收服务端。
        server: uploadServer,
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker7',
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });

    // 当有文件添加进来的时候
    uploader7.on( 'fileQueued', function( file ) {
        $list7.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
            '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader7.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id ),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%">' +
                '</div>' +
                '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader7.on( 'uploadSuccess', function( file , response) {
        // console.log(response);
        // var json = eval('('+response+')');
        var json = response;
        if(json.statusCode == 200){
            $( '#pic5' ).val(json.filename);
            $("#pic5-preview").html("<img src='"+basePath+json.filename+"'/ height='100px' width='100px'>");
        }
        $( '#'+file.id ).find('h4.info').text('');
        $( '#'+file.id ).find('p.state').text('');
    });

    uploader7.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader7.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader7.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn7.text('暂停上传');
        } else {
            $btn7.text('开始上传');
        }
    });

    $btn7.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader7.stop();
        } else {
            uploader7.upload();
        }
    });
});